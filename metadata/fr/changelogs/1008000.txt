* Barre discrèt réparée sur les versions d'Androids récents (merci à @dkanada)
* Support d'API 29 "split-permissions" : un emplacement précit implique maintenant un emplacement grossier
* Règle de backup pour sauvegarder les dépots
